#-------------------------------------------------
#
# Project created by QtCreator 2016-01-29T14:04:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NATO
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    phoneticstring.cpp \
    outputwindow.cpp

HEADERS  += mainwindow.h \
    phoneticstring.h \
    outputwindow.h

FORMS    += mainwindow.ui \
    outputwindow.ui
