#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionNATO_to_Regular,     SIGNAL(triggered()), this, SLOT(setModeToNATO()    ));
    connect(ui->actionRegular_to_NATO,     SIGNAL(triggered()), this, SLOT(setModeToRegular() ));
    connect(ui->actionAbout,               SIGNAL(triggered()), this, SLOT(displayAbout()     ));

    conversionMode = 1;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_convertButton_clicked()
{
    if(conversionMode == 1)
    {
        OutputWindow *w = new OutputWindow;
        w->setAttribute(Qt::WA_DeleteOnClose);
        PhoneticString phoneticString(ui->textEdit->toPlainText());
        output = QString::fromStdString(phoneticString.getString());
        w->getOutput        (output);
        w->getConversionMode(conversionMode);
        w->show();
    }
    else
    {
        // put reverse code here
    }
}

void MainWindow::setModeToNATO()
{
    conversionMode = 1;
}

void MainWindow::setModeToRegular()
{
    conversionMode = 0;
}

void MainWindow::displayAboutWindow()
{

}
