#ifndef OUTPUTWINDOW_H
#define OUTPUTWINDOW_H

#include <QWidget>

namespace Ui {
class OutputWindow;
}

class OutputWindow : public QWidget
{
    Q_OBJECT

public:
    explicit OutputWindow(QWidget *parent = 0);
    ~OutputWindow();
    void getOutput(QString output);
    void getConversionMode  (int conversionMode);

private:
    Ui::OutputWindow *ui;
    int conversionMode;
};

#endif // OUTPUTWINDOW_H
