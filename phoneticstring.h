#ifndef PHONETICSTRING_H
#define PHONETICSTRING_H


#include <QString>
#include <vector>
#include <iostream>
#include <boost/algorithm/string.hpp>

class PhoneticString
{
public:
    PhoneticString(QString                  message);
    PhoneticString(std::string              message);
    PhoneticString(char*                    message);
    PhoneticString(std::vector<char>        message);

    ~PhoneticString();

    std::string  getString();

    void         modifyString(std::string             );
    void         modifyString(QString                 );
    void         modifyString(char*                   );
    void         modifyString(std::vector<char>       );

    static std::string convertCharToNATO(char input);
    static std::string convertCharToNATO(QChar input);
    static char        convertNATOToChar(std::string);

private:
    std::string* NATO_PHONETIC_STRING;
};

#endif // PHONETICSTRING_H
