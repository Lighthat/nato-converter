#include "phoneticstring.h"

PhoneticString::PhoneticString(QString message)
{
    QString convertedString;
    for(int i = 0; i < message.size(); ++i)
    {
        //message.at(i) returns QChar
        //convert QChar to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message.at(i))));
        convertedString.append(" ");
    }
    NATO_PHONETIC_STRING = new std::string(convertedString.toStdString());
}

PhoneticString::PhoneticString(std::string message)
{
    QString convertedString;
    for(unsigned int i = 0; i < message.size(); ++i)
    {
        //message[i] returns std::char
        //convert std::char to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message[i])));
        convertedString.append(" ");
    }
    NATO_PHONETIC_STRING = new std::string(convertedString.toStdString());
}

PhoneticString::PhoneticString(char* message)
{
    QString convertedString;
    unsigned int i = 0;
    while(message[i] != '\0')
    {
        //message[i] returns std::char
        //convert std::char to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message[i])));
        convertedString.append(" ");
    }
    NATO_PHONETIC_STRING = new std::string(convertedString.toStdString());
}

PhoneticString::PhoneticString(std::vector<char> message)
{
    QString convertedString;
    for(unsigned int i = 0; i < message.size(); ++i)
    {
        //message.at(i) returns std::char
        //convert std::char to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message.at(i))));
        convertedString.append(" ");
    }
    NATO_PHONETIC_STRING = new std::string(convertedString.toStdString());
}

PhoneticString::~PhoneticString()
{
    delete NATO_PHONETIC_STRING;
}

std::string PhoneticString::getString()
{
    return *NATO_PHONETIC_STRING;
}

void PhoneticString::modifyString(std::string message)
{
    QString convertedString;
    for(unsigned int i = 0; i < message.size(); ++i)
    {
        //message.at(i) returns QChar
        //convert QChar to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message.at(i))));
        convertedString.append(" ");
    }
    *NATO_PHONETIC_STRING = convertedString.toStdString();
}

void PhoneticString::modifyString(QString message)
{
    QString convertedString;
    for(int i = 0; i < message.size(); ++i)
    {
        //message[i] returns std::char
        //convert std::char to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message[i])));
        convertedString.append(" ");
    }
    *NATO_PHONETIC_STRING = convertedString.toStdString();
}

void PhoneticString::modifyString(char* message)
{
    QString convertedString;
    unsigned int i = 0;
    while(message[i] != '\0')
    {
        //message[i] returns std::char
        //convert std::char to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message[i])));
        convertedString.append(" ");
    }
    *NATO_PHONETIC_STRING = convertedString.toStdString();
}

void PhoneticString::modifyString(std::vector<char> message)
{
    delete NATO_PHONETIC_STRING;
    QString convertedString;
    for(unsigned int i = 0; i < message.size(); ++i)
    {
        //message.at(i) returns std::char
        //convert std::char to corresponding NATO std::string
        //append to convertedString by converting to a QString
        //append a space for the next word
        convertedString.append(QString::fromStdString(convertCharToNATO(message.at(i))));
        convertedString.append(" ");
    }
    NATO_PHONETIC_STRING = new std::string(convertedString.toStdString());
}

std::string PhoneticString::convertCharToNATO(char input)
{
    switch(input)
    {
    case 'a':
    case 'A':
        return "Alpha";
        break;
    case 'b':
    case 'B':
        return "Bravo";
        break;
    case 'c':
    case 'C':
        return "Charlie";
        break;
    case 'd':
    case 'D':
        return "Delta";
        break;
    case 'e':
    case 'E':
        return "Echo";
        break;
    case 'f':
    case 'F':
        return "Foxtrot";
        break;
    case 'g':
    case 'G':
        return "Golf";
        break;
    case 'h':
    case 'H':
        return "Hotel";
        break;
    case 'i':
    case 'I':
        return "India";
        break;
    case 'j':
    case 'J':
        return "Juliett";
        break;
    case 'k':
    case 'K':
        return "Kilo";
        break;
    case 'l':
    case 'L':
        return "Lima";
        break;
    case 'm':
    case 'M':
        return "Mike";
        break;
    case 'n':
    case 'N':
        return "November";
        break;
    case 'o':
    case 'O':
        return "Oscar";
        break;
    case 'p':
    case 'P':
        return "Papa";
        break;
    case 'q':
    case 'Q':
        return "Quebec";
        break;
    case 'r':
    case 'R':
        return "Romeo";
        break;
    case 's':
    case 'S':
        return "Sierra";
        break;
    case 't':
    case 'T':
        return "Tango";
        break;
    case 'u':
    case 'U':
        return "Uniform";
        break;
    case 'v':
    case 'V':
        return "Victor";
        break;
    case 'w':
    case 'W':
        return "Whiskey";
        break;
    case 'x':
    case 'X':
        return "Xray";
        break;
    case 'y':
    case 'Y':
        return "Yankee";
        break;
    case 'z':
    case 'Z':
        return "Zulu";
        break;
    case ' ':
        return "|";
        break;
    default:
        return "[INVALID]";
        break;
    }
}

std::string PhoneticString::convertCharToNATO(QChar input)
{
    switch(input.toLatin1())
    {
    case 'a':
    case 'A':
        return "Alpha";
        break;
    case 'b':
    case 'B':
        return "Bravo";
        break;
    case 'c':
    case 'C':
        return "Charlie";
        break;
    case 'd':
    case 'D':
        return "Delta";
        break;
    case 'e':
    case 'E':
        return "Echo";
        break;
    case 'f':
    case 'F':
        return "Foxtrot";
        break;
    case 'g':
    case 'G':
        return "Golf";
        break;
    case 'h':
    case 'H':
        return "Hotel";
        break;
    case 'i':
    case 'I':
        return "India";
        break;
    case 'j':
    case 'J':
        return "Juliett";
        break;
    case 'k':
    case 'K':
        return "Kilo";
        break;
    case 'l':
    case 'L':
        return "Lima";
        break;
    case 'm':
    case 'M':
        return "Mike";
        break;
    case 'n':
    case 'N':
        return "November";
        break;
    case 'o':
    case 'O':
        return "Oscar";
        break;
    case 'p':
    case 'P':
        return "Papa";
        break;
    case 'q':
    case 'Q':
        return "Quebec";
        break;
    case 'r':
    case 'R':
        return "Romeo";
        break;
    case 's':
    case 'S':
        return "Sierra";
        break;
    case 't':
    case 'T':
        return "Tango";
        break;
    case 'u':
    case 'U':
        return "Uniform";
        break;
    case 'v':
    case 'V':
        return "Victor";
        break;
    case 'w':
    case 'W':
        return "Whiskey";
        break;
    case 'x':
    case 'X':
        return "Xray";
        break;
    case 'y':
    case 'Y':
        return "Yankee";
        break;
    case 'z':
    case 'Z':
        return "Zulu";
        break;
    case ' ':
        return "|";
        break;
    default:
        return "[INVALID]";
        break;
    }
}

char PhoneticString::convertNATOToChar(std::string input)
{
    boost::algorithm::to_lower(input);
    switch(input)
    {

    }
}
