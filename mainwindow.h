#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "phoneticstring.h"
#include "outputwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_convertButton_clicked();

    void setModeToNATO();
    void setModeToRegular();
    void displayAboutWindow();


private:
    Ui::MainWindow *ui;
    QString output;
    int conversionMode;
};

#endif // MAINWINDOW_H
