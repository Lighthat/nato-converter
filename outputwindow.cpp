#include "outputwindow.h"
#include "ui_outputwindow.h"

OutputWindow::OutputWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OutputWindow)
{
    ui->setupUi(this);
    ui->textEdit->setReadOnly(true);
}

OutputWindow::~OutputWindow()
{
    delete ui;
}

void OutputWindow::getOutput(QString output)
{
    ui->textEdit->append(output);
}

void OutputWindow::getConversionMode(int conversionMode)
{
    this->conversionMode = conversionMode;
}
